## This is Python an utility to monitor system resources  

Utility runs by default every 30 seconds.  
Once run, the script stops by default after 20 runs.  
JSON file named snapshot wil be created by default   


### There are three arguments: 
```python
   -f to overwrite default file name  
   -n to overwrite default run count  
   -h to display help information  
   -i to interval betwen run
```

### Usage   

```python
snapshot -i 1   
snapshot -f test.json
snapshot -n 5
```
