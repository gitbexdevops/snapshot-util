from setuptools import setup, find_packages

setup(
    name="snapshot",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "snapshot = snapshot.snapshot:main",
        ],
    },
    install_requires=[
        # put your requirements separated by comma here
    ],
    version="0.1",
    author="Bekhruz Mirrakhimov",
    author_email="bekhruz_mirrakhimov@epam.com",
    description="Epam utility task",
    license="MIT",
)
