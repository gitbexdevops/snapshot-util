import json
import psutil
import argparse
from datetime import datetime
import time
import os
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=30)
parser.add_argument("-f", help="Output file name", default="snapshot.json")
parser.add_argument("-n", help="Quantity of snapshot to output", default=20)
args = parser.parse_args()

data = []


def get_cpu_dict():
    cpu_dict = dict()
    cpu_dict['user ='] = psutil.cpu_times().idle
    cpu_dict['system ='] = psutil.cpu_times().system
    cpu_dict['idle ='] = psutil.cpu_times().idle
    return cpu_dict


class Memory:
    def __init__(self, psutil_name):
        self.psutil_name = psutil_name

    def get_mem(self):
        dictionary = dict()
        mem = self.psutil_name()
        dictionary['total'] = int(mem.total / 1024)
        dictionary['free'] = int(mem.free / 1024)
        dictionary['used'] = int(mem.used / 1024)
        return dictionary


get_mem_dict = Memory(psutil.virtual_memory)
get_swap_dict = Memory(psutil.swap_memory)


def get_tasks_info():
    # result_dictionary = defaultdict(int)
    # count = 0
    # for pid in psutil.pids():
    #     result_dictionary[psutil.Process(pid).status()] += 1
    #     count += 1
    # result_dictionary['total'] = count

    result_dictionary = {}
    pid_list = psutil.pids()
    count = 0
    count_of_sleep = 0
    count_of_running = 0
    count_of_stopped = 0
    count_of_zombie = 0
    for i in pid_list:
        count += 1
        status = psutil.Process(i).status()
        if status == "sleeping":
            count_of_sleep += 1
        elif status == "running":
            count_of_running += 1
        elif status == "stopped":
            count_of_stopped += 1
        elif status == "zombie":
            count_of_zombie += 1
    result_dictionary["total"] = count
    result_dictionary["running"] = count_of_running
    result_dictionary["sleeping"] = count_of_sleep
    result_dictionary["stopped"] = count_of_stopped
    result_dictionary["zombie"] = count_of_zombie
    return result_dictionary


def get_system_info():
    sys_info = dict()
    now = datetime.now()
    sys_info['Tasks'] = get_tasks_info()
    sys_info['%CPU'] = get_cpu_dict()
    sys_info['KiB Mem'] = get_mem_dict.get_mem()
    sys_info['KiB Swap'] = get_swap_dict.get_mem()
    sys_info['Timestamp'] = now.strftime("%H:%M:%S")
    return sys_info


def interval():
    data.append(get_system_info())  # insert json file data into an array
    print(get_system_info(), end="\r")


def main():
    """Snapshot tool."""
    snapshot_count = 0
    while snapshot_count < int(args.n):
        interval()
        time.sleep(args.i)
        os.system('clear')
        snapshot_count += 1
    with open(args.f, "w") as file:
        json.dump(data, file, indent=2)
        file.close()


if __name__ == "__main__":
    main()
